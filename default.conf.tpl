server {
  listen ${LISTEN_PORT};

  gzip on;
  gzip_types      text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;
  gzip_proxied    any;

  location /static {
    alias /vol/static;
  }

  location / {
    uwsgi_pass           ${APP_HOST}:${APP_PORT};
    include              /etc/nginx/uwsgi_params;
    client_max_body_size 10M;
  }
}